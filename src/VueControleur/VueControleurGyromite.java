package VueControleur;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.jar.JarEntry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.math.*;

import modele.deplacements.ColonneControl;
import modele.deplacements.Controle4Directions;
import modele.deplacements.Direction;
import modele.plateau.*;


/** Cette classe a deux fonctions :
 *  (1) Vue : proposer une représentation graphique de l'application (cases graphiques, etc.)
 *  (2) Controleur : écouter les évènements clavier et déclencher le traitement adapté sur le modèle (flèches direction Pacman, etc.))
 *
 */
public class VueControleurGyromite extends JFrame implements Observer {
    private Jeu jeu; // référence sur une classe de modèle : permet d'accéder aux données du modèle pour le rafraichissement, permet de communiquer les actions clavier (ou souris)

    private int screen_width; // taille de la grille affichée
    private int screen_height;

    private Point screen_offset = new Point();
    
    // icones affichées dans la grille
    private ImageIcon icoHero;
    private ImageIcon icoBot;
    private ImageIcon icoVide;
    private ImageIcon icoMur;
    private ImageIcon icoCorde;
    private ImageIcon icoSmicks;
    private ImageIcon icoPoutreHorizontale;
    private ImageIcon icoPoutreVerticale;
    private ImageIcon icoToyoBordGauche;
    private ImageIcon icoToyoBordDroit;
    private ImageIcon[] icoToyoTenuHaut = new ImageIcon[2];
    private ImageIcon[] icoToyoTenuMilieu = new ImageIcon[2];
    private ImageIcon[] icoToyoTenuBas = new ImageIcon[2];
    private ImageIcon[] icoToyoHautPlein = new ImageIcon[2];
    private ImageIcon[] icoToyoMilieuPlein = new ImageIcon[2];
    private ImageIcon[] icoToyoBasPlein = new ImageIcon[2];
    private ImageIcon[] icoToyoHautMoitie = new ImageIcon[2];
    private ImageIcon[] icoToyoBasMoitie = new ImageIcon[2];
    private ImageIcon icoTNT;

    private int tileSize;

    private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associée à une icône, suivant ce qui est présent dans le modèle)
    private JLabel topbar;

    public VueControleurGyromite(Jeu _jeu) {
        screen_width = 16;
        screen_height = 12;
        jeu = _jeu;

        tileSize = 32;

        chargerLesIcones();
        placerLesComposantsGraphiques();
        ajouterEcouteurClavier();
    }

    private void ajouterEcouteurClavier() {
        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode()) {  // on regarde quelle touche a été pressée
                    case KeyEvent.VK_LEFT : Controle4Directions.getInstance().setDirectionCourante(Direction.gauche); break;
                    case KeyEvent.VK_RIGHT : Controle4Directions.getInstance().setDirectionCourante(Direction.droite); break;
                    case KeyEvent.VK_DOWN : Controle4Directions.getInstance().setDirectionCourante(Direction.bas); break;
                    case KeyEvent.VK_UP : Controle4Directions.getInstance().setDirectionCourante(Direction.haut); break;
                    case KeyEvent.VK_ESCAPE : System.exit(0); break; //TODO: Trouver une meilleure fermeture du jeu ?
                    case KeyEvent.VK_A : ColonneControl.getBlueInstance().toggleDirection(); break;
                    case KeyEvent.VK_B : ColonneControl.getRedInstance().toggleDirection(); break;
                }
            }
        });
    }


    private void chargerLesIcones() {
    // Sprites
        icoHero = chargerIcone("Images/player_ca.png", 0, 0, 35, 40);
        icoBot = chargerIcone("Images/smick_ca.png", 0, 0, 35, 40);
    // Tiles
        icoCorde = chargerIcone("Images/tileset.png", 16, 0, 16, 16);
        icoMur = chargerIcone("Images/tileset.png", 32, 0, 16, 16);
        icoVide = chargerIcone("Images/tileset.png", 48, 0, 16, 16);
        icoPoutreHorizontale = chargerIcone("Images/tileset.png", 0, 0, 16, 16);
        icoPoutreVerticale = chargerIcone("Images/tileset.png", 0, 16, 16, 16);
        
        icoToyoBordGauche = chargerIcone("Images/tileset.png", 16, 16, 16, 16);    
        icoToyoBordDroit = chargerIcone("Images/tileset.png", 32, 16, 16, 16);
        // Toyo Bleu
        icoToyoBasMoitie[0] = chargerIcone("Images/tileset.png", 64, 48, 16, 16);
        icoToyoBasPlein[0] = chargerIcone("Images/tileset.png", 32, 48, 16, 16);
        icoToyoHautMoitie[0] = chargerIcone("Images/tileset.png", 48, 48, 16, 16);
        icoToyoHautPlein[0] = chargerIcone("Images/tileset.png", 0, 48, 16, 16);
        icoToyoMilieuPlein[0] = chargerIcone("Images/tileset.png", 16, 48, 16, 16);
        icoToyoTenuBas[0] = chargerIcone("Images/tileset.png", 32, 32, 16, 16);
        icoToyoTenuHaut[0] = chargerIcone("Images/tileset.png", 0, 32, 16, 16);
        icoToyoTenuMilieu[0] = chargerIcone("Images/tileset.png", 16, 32, 16, 16);
        // Toyo Rouge
        icoToyoBasMoitie[1] = chargerIcone("Images/tileset.png", 64, 80, 16, 16);
        icoToyoBasPlein[1] = chargerIcone("Images/tileset.png", 32, 80, 16, 16);
        icoToyoHautMoitie[1] = chargerIcone("Images/tileset.png", 48, 80, 16, 16);
        icoToyoHautPlein[1] = chargerIcone("Images/tileset.png", 0, 80, 16, 16);
        icoToyoMilieuPlein[1] = chargerIcone("Images/tileset.png", 16, 80, 16, 16);
        icoToyoTenuBas[1] = chargerIcone("Images/tileset.png", 32, 64, 16, 16);
        icoToyoTenuHaut[1] = chargerIcone("Images/tileset.png", 0, 64, 16, 16);
        icoToyoTenuMilieu[1] = chargerIcone("Images/tileset.png", 16, 64, 16, 16);

        // TNT
        icoTNT = chargerIcone("Images/bomb_ca.png", 8, 0, 48, 48);
    }

    private JComponent creerTopbar(Point size) {
        topbar = new JLabel("Score : 0     |     Time : 999");
        topbar.setPreferredSize(new Dimension(size.x, size.y));
        topbar.setBackground(new Color(0, 0, 0));
        topbar.setForeground(new Color(255, 255, 255));
        topbar.setHorizontalAlignment(JLabel.CENTER);
        topbar.setOpaque(true);

        return topbar;
    }

    private JComponent creerGrilleJLabels(Point size) {
        JComponent grilleJLabels = new JPanel(new GridLayout(screen_height, screen_width)); // grilleJLabels va contenir les cases graphiques et les positionner sous la forme d'une grille
        grilleJLabels.setPreferredSize(new Dimension(size.x, size.y)); // Pour retirer les bords blanc
        
        tabJLabel = new JLabel[screen_width][screen_height];

        for (int y = 0; y < screen_height; y++) {
            for (int x = 0; x < screen_width; x++) {
                JLabel jlab = new JLabel();

                tabJLabel[x][y] = jlab; // on conserve les cases graphiques dans tabJLabel pour avoir un accès pratique à celles-ci (voir mettreAJourAffichage() )
                grilleJLabels.add(jlab);
            }
        }
        return grilleJLabels;
    }


    private void placerLesComposantsGraphiques() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        Point gameDimension = new Point(screen_width * tileSize, screen_height * tileSize);
        Point topbarDimension = new Point(gameDimension.x, tileSize) ;

        mainPanel.add(creerGrilleJLabels(gameDimension), BorderLayout.CENTER);
        mainPanel.add(creerTopbar(topbarDimension), BorderLayout.NORTH);
        
        mainPanel.setPreferredSize(new Dimension(gameDimension.x, gameDimension.y + topbarDimension.y));

        // Sera utile pour quand on voudra afficher les écrans de mort et de victoire
        //mainPanel.getComponent(1).setVisible(false); 

        add(mainPanel);
        
        setTitle("Gyromite");
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // permet de terminer l'application à la fermeture de la fenêtre
        pack(); // Pour retirer les bords blanc
        setVisible(true);
    }

    private void updateTopbar() {
        topbar.setText("Score : " + jeu.getScore() + "     |     Time : " + jeu.getTimeRemaining());
    }
    
    /**
     * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté de la vue (tabJLabel)
     */
    private void mettreAJourAffichage() {
        Entite testedEntity;
        Point ePos = jeu.getHector().getTileIndex();
        computeScreenOffset(ePos.x, ePos.y);
        Point coord;

        // Affichage tuile
        for (int i = 0; i < screen_width; i++) {
            for (int j = 0; j < screen_height; j++) {
            //     // si transparence : images avec canal alpha + dessins manuels (voir ci-dessous + créer composant qui redéfinie paint(Graphics g)), se documenter
            //     //BufferedImage bi = getImage("Images/smick.png", 0, 0, 20, 20);
            //     //tabJLabel[x][y].getGraphics().drawImage(bi, 0, 0, null);

                coord = screen2map(i, j);
                testedEntity = jeu.objetALaPosition(coord);

                if (testedEntity instanceof Mur) {
                    tabJLabel[i][j].setIcon(icoMur);
                } else if (testedEntity instanceof Poutre) {
                    tabJLabel[i][j].setIcon(getCorrectPoutreIcon(coord.x, coord.y, (Poutre)testedEntity));
                } else if (testedEntity instanceof Corde) {
                    tabJLabel[i][j].setIcon(icoCorde);
                } else if (testedEntity instanceof TNT) {
                    tabJLabel[i][j].setIcon(icoTNT);
                } else {
                    tabJLabel[i][j].setIcon(icoVide);
                }
            }
        }

        // Affichage mobs
        ePos = map2screen(ePos);
        if(estDansScreen(ePos))
            tabJLabel[ePos.x][ePos.y].setIcon(icoHero);
        
        for(Bot smick : jeu.getSmicks()) {
            ePos = map2screen(smick.getTileIndex());
            if(estDansScreen(ePos))
                tabJLabel[ePos.x][ePos.y].setIcon(icoBot);
        }

        for(Colonne col : jeu.getToyo()) {
            coord = col.getTileIndex();
            ePos = map2screen(coord);
            if(estDansScreen(ePos))
                tabJLabel[ePos.x][ePos.y].setIcon(getCorrectToyoIcon(coord.x, coord.y, col));
        }

        updateTopbar();
 
    }

    @Override
    public void update(Observable o, Object arg) {
        mettreAJourAffichage();
        /*
        SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        mettreAJourAffichage();
                    }
                }); 
        */

    }

    private boolean estDansScreen(int x, int y) {
        return x >= 0 && x < screen_width && y >= 0 && y < screen_height;
    }
    
    private boolean estDansScreen(Point p) {
        return estDansScreen(p.x, p.y);
    }

    private void computeScreenOffset(int player_x, int player_y) {
        screen_offset.x = (int)Math.floor(-0.5f * screen_width) + player_x;
        screen_offset.y = (int)Math.floor(-0.5f * screen_height) + player_y;

        if(screen_offset.x < 0)
            screen_offset.x = 0;
        else if(screen_offset.x + screen_width >= jeu.SIZE_X)
            screen_offset.x = jeu.SIZE_X - screen_width;
        if(screen_offset.y < 0)
            screen_offset.y = 0;
        else if(screen_offset.y + screen_height >= jeu.SIZE_Y)
            screen_offset.y = jeu.SIZE_Y - screen_height;
    }

    private Point map2screen(int x, int y) {
        return new Point(x - screen_offset.x, y - screen_offset.y);
    }

    private Point screen2map(int x, int y) {
        return new Point(x + screen_offset.x, y + screen_offset.y);
    }
    
    private Point map2screen(Point p) {
        return map2screen(p.x, p.y);
    }

    private Point screen2map(Point p) {
        return screen2map(p.x, p.y);
    }

    // chargement d'une sous partie de l'image
    private ImageIcon chargerIcone(String urlIcone, int x, int y, int w, int h) {
        // charger une sous partie de l'image à partir de ses coordonnées dans urlIcone
        BufferedImage bi = getSubImage(urlIcone, x, y, w, h);
        // adapter la taille de l'image a la taille du composant (ici : 20x20)
        return new ImageIcon(bi.getScaledInstance(tileSize, tileSize, java.awt.Image.SCALE_SMOOTH));
    }

    private BufferedImage getSubImage(String urlIcone, int x, int y, int w, int h) {
        BufferedImage image = null;

        try {
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleurGyromite.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        BufferedImage bi = image.getSubimage(x, y, w, h);
        return bi;
    }

    private ImageIcon getCorrectToyoIcon(int x, int y, Colonne c) {
        // Si dessus est pas Colonne
        if(!(jeu.objetALaPosition(new Point(x, y-1)) instanceof Colonne))
            if(jeu.objetALaPosition(new Point(x + 1, y)) instanceof Poutre
            || jeu.objetALaPosition(new Point(x - 1, y)) instanceof Poutre)
                return icoToyoTenuHaut[c.estBleu() ? 0 : 1];
            else
                return icoToyoHautPlein[c.estBleu() ? 0 : 1];
        // Sinon si dessous pas Colonne
        else if(!(jeu.objetALaPosition(new Point(x, y+1)) instanceof Colonne))
            if(jeu.objetALaPosition(new Point(x + 1, y)) instanceof Poutre
            || jeu.objetALaPosition(new Point(x - 1, y)) instanceof Poutre)
                return icoToyoTenuBas[c.estBleu() ? 0 : 1];
            else
                return icoToyoBasPlein[c.estBleu() ? 0 : 1];
        // Sinon alors en dessous et au dessus sont Colonne
        else
            if(jeu.objetALaPosition(new Point(x+1, y)) instanceof Poutre 
            || jeu.objetALaPosition(new Point(x-1, y)) instanceof Poutre)
                return icoToyoTenuMilieu[c.estBleu() ? 0 : 1];
            else
                return icoToyoMilieuPlein[c.estBleu() ? 0 : 1];
    }

    private ImageIcon getCorrectPoutreIcon(int x, int y, Poutre p) {
        if(jeu.objetALaPosition(new Point(x + 1, y)) instanceof Colonne)
            return icoToyoBordGauche;
        else if(jeu.objetALaPosition(new Point(x - 1, y)) instanceof Colonne)
            return icoToyoBordDroit;
        else if(p.estHorizontale())
            return icoPoutreHorizontale;
        else
            return icoPoutreVerticale;
            
    }

}
