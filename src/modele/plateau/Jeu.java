/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

import modele.deplacements.Controle4Directions;
import modele.deplacements.Direction;
import modele.deplacements.Gravite;
import modele.deplacements.Ordonnanceur;
import modele.deplacements.ColonneControl;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.File;
import java.util.Scanner;

/** Actuellement, cette classe gère les postions
 * (ajouter conditions de victoire, chargement du plateau, etc.)
 */
public class Jeu {

    public enum tileID {
        VIDE(0), MUR(1), POUTRE_H(2), POUTRE_V(3),
        CORDE(4), TOYO_BLEU(5), TOYO_ROUGE(6), HECTOR(7), SMICK(8), 
        TNT(9), RADIS(10);

        public int value;

        tileID(int v) {
            value = v;
        }
    };

    public static int SIZE_X;
    public static int SIZE_Y;

    // compteur de déplacements horizontal et vertical (1 max par défaut, à chaque pas de temps)
    private HashMap<Entite, Integer> cmptDeplH = new HashMap<Entite, Integer>();
    private HashMap<Entite, Integer> cmptDeplV = new HashMap<Entite, Integer>();

    // Structures d'entités
    private Heros hector;
    private ArrayList<Bot> smicks = new ArrayList<Bot>();
    private ArrayList<Colonne> toyos = new ArrayList<Colonne>();

    private HashMap<Entite, Point> map = new  HashMap<Entite, Point>(); // permet de récupérer la position d'une entité à partir de sa référence
    private Entite[][] grilleEntites; // permet de récupérer une entité à partir de ses coordonnées

    // Fonctionnement du jeu
    private Ordonnanceur ordonnanceur = new Ordonnanceur(this);
    private Gravite g = new Gravite();
    private int total_tnt;
    private int tnt_count;

    private int current_level;
    private int time_remaining;
    private int score;
    private boolean gameOver=false;

    boolean gameIsOver;
    boolean levelCleared;



    public Jeu() {
        gameIsOver = false;
        levelCleared = false;
        current_level = 0;
        score = 0;
        initialisationDesEntites();
    }

    public void resetCmptDepl() {
        cmptDeplH.clear();
        cmptDeplV.clear();
    }

    public void start(long _pause) {
        ordonnanceur.start(_pause);
    }

    public void stop() {
        ordonnanceur.stop();
    }
    
    public Entite[][] getGrille() {
        return grilleEntites;
    }
    
    public Heros getHector() {
        return hector;
    }

    public ArrayList<Bot> getSmicks() {
        return smicks;
    }

    public ArrayList<Colonne> getToyo() {
        return toyos;
    }

    public int getScore() {
        return score;
    }

    public int getTimeRemaining() {
        return time_remaining;
    }

    private void generateToyo(boolean estBleu, boolean estEnBas, int x, int y) {
        
        int offset = estEnBas?1:0;

        for(int i = 0; i < 3; i++) {
            Colonne colTmp = new Colonne(this, x, y + offset - i, estBleu);
            toyos.add(colTmp);
            if(estBleu)
                ColonneControl.getBlueInstance().addEntiteDynamique(colTmp, estEnBas);
            else
                ColonneControl.getRedInstance().addEntiteDynamique(colTmp, estEnBas);
        }
    }
    
    private void initialisationDesEntites() {
        try {
            ordonnanceur.add(g);
            ordonnanceur.add(Controle4Directions.getInstance());
            ordonnanceur.add(ColonneControl.getBlueInstance());
            ordonnanceur.add(ColonneControl.getRedInstance());
            
            chargerNiveau("Levels/niveau"+current_level+".lvl");

            
            
        }   
        catch(Exception e) {
            System.out.println("impossilbe de charger le niveau Levels/niveau"+current_level+".lvl");
        }

    }

    private void clearMemory() {
        hector = null;
        for(Colonne e : toyos) {
            e = null;
        }
        toyos.clear();
        for(Bot e : smicks) {
            e = null;
        }
        smicks.clear();
        
        Controle4Directions.getInstance().clearEntiteDYnamique();
        ColonneControl.getRedInstance().clearEntiteDYnamique();
        ColonneControl.getBlueInstance().clearEntiteDYnamique();
    }

    private void chargerNiveau(String path) throws Exception {
        
        File file = new File(path);
        Scanner scan = new Scanner(file);
        int[] infoMap = new int[2];
        int index = 0;

        if(scan.hasNextInt()) SIZE_X = scan.nextInt(); // Dimension X
        if(scan.hasNextInt()) SIZE_Y = scan.nextInt(); // Dimension Y
        if(scan.hasNextInt()) infoMap[0] = scan.nextInt(); // Direction toyo bleu
        if(scan.hasNextInt()) infoMap[1] = scan.nextInt(); // Direction toyo rouge

        grilleEntites = new Entite[SIZE_X][SIZE_Y];
        int[] rawMap = new int[SIZE_X * SIZE_Y];     
        
        while(scan.hasNextInt()) {
            rawMap[index] = scan.nextInt();
            index++;
        }

        scan.close();
        
        time_remaining = 999;
        gameIsOver = false;
        levelCleared = false;
        
        clearMemory();

        for(int i = 0; i < SIZE_Y; i++) {
            for(int j = 0; j < SIZE_X; j++) {
                tileID c = tileID.values()[rawMap[j + i * SIZE_X]];
                switch(c) {
                    case VIDE:
                        break;
                    case MUR:
                        addEntite(new Mur(this), j, i);
                        break;
                    case POUTRE_H:
                        addEntite(new Poutre(this, true), j, i);
                        break;
                    case POUTRE_V:
                        addEntite(new Poutre(this, false), j, i);
                        break;
                    case CORDE:
                        addEntite(new Corde(this), j, i);
                        break;
                    case TOYO_BLEU:
                        generateToyo(true, infoMap[0]==0, j, i);
                        break;
                    case TOYO_ROUGE:
                        generateToyo(false, infoMap[1]==0, j, i);
                        break;
                    case HECTOR:
                        hector = new Heros(this, j, i-1);
                        g.addEntiteDynamique(hector);
                        Controle4Directions.getInstance().addEntiteDynamique(hector);
                        break;
                    case SMICK:
                        Bot smick = new Bot(this, j, i);
                        smicks.add(smick);
                        ordonnanceur.add(smick.IAdeplacement());
                        g.addEntiteDynamique(smick);
                        break;
                    case TNT:
                        addEntite(new TNT(this), j, i);
                        total_tnt++;
                        break;
                    case RADIS:
                        break;
                }
            }

        }
    }

    public boolean checkLevelUpdate() {
        if(gameIsOver) {
            score -= tnt_count * 100;
            try {chargerNiveau("Levels/niveau" + current_level +".lvl");}
            catch(Exception e) {}
        }
        else if(levelCleared) {
            current_level++;
            score += time_remaining * 10;
            if(current_level < 2)
                try {chargerNiveau("Levels/niveau"+current_level+".lvl");}
                catch(Exception e) {}
            else
                return true;
        }
        time_remaining -= 1;
        return false;
    }

    private void addEntite(Entite e, int x, int y) {
        grilleEntites[x][y] = e;
        map.put(e, new Point(x, y));
    }
    
    /** Permet par exemple a une entité  de percevoir sont environnement proche et de définir sa stratégie de déplacement
     *
     */
    public Entite regarderDansLaDirection(Entite e, Direction d) {
        Point positionEntite = positionDeLObjet(e);
        return objetALaPosition(calculerPointCible(positionEntite, d));
    }

    public Entite regardeEnDiagonale(Entite e, Direction gd, Direction hb) {
        Point positionEntite;
        if(e instanceof Heros || e instanceof Bot) {
            positionEntite = ((EntiteDynamique) e).getTileIndex();
        }
        else {
            positionEntite = map.get(e);
        }
        Entite ret;
        if (hb==Direction.bas){
            positionEntite.translate(0,1);
            ret = objetALaPosition(calculerPointCible(positionEntite, gd));
            positionEntite.translate(0, -1);
        }
        else{
            positionEntite.translate(0,-1);
            ret = objetALaPosition(calculerPointCible(positionEntite, gd));
            positionEntite.translate(0, 1);
        }
        return ret;
    }
    
    
    /** Si le déplacement de l'entité est autorisé (pas de mur ou autre entité), il est réalisé
     * Sinon, rien n'est fait.
     */
    public boolean deplacerEntite(Entite e, Direction d) {
        boolean retour = false;
        
        Point pCourant = positionDeLObjet(e);
        
        Point pCible = calculerPointCible(pCourant, d);
        if (contenuDansGrille(pCible) && (objetALaPosition(pCible) == null || objetALaPosition(pCible).peutServirDeSupport() == false || e instanceof Colonne)) { // a adapter (collisions murs, etc.)
            // compter le déplacement : 1 deplacement horizontal et vertical max par pas de temps par entité
            switch (d) {
                case bas:
                case haut:
                    if (cmptDeplV.get(e) == null) {
                        cmptDeplV.put(e, 1);
                        if(e instanceof Colonne)
                            handleCrush(e, d, pCible);
                        retour = true;
                    }
                    break;
                case gauche:
                case droite:
                    if (cmptDeplH.get(e) == null) {
                        cmptDeplH.put(e, 1);
                        retour = true;

                    }
                    break;
            }
        }

        if (retour) {
            deplacerEntite(pCourant, pCible, e);
            for(Bot s: smicks){
                if (hector.currentTile()==s.currentTile())
                    GameOver();
            }
        }

        return retour;
    }
    
    
    private Point calculerPointCible(Point pCourant, Direction d) {
        Point pCible = null;
        //On en déduit que les entités ont un champ de vision qui s'étend à un block ici
        switch(d) {
            case haut: pCible = new Point(pCourant.x, pCourant.y - 1); break;
            case bas : pCible = new Point(pCourant.x, pCourant.y + 1); break;
            case gauche : pCible = new Point(pCourant.x - 1, pCourant.y); break;
            case droite : pCible = new Point(pCourant.x + 1, pCourant.y); break;     
            
        }
        
        return pCible;
    }

    private void handleTnt(Point pCourant, Point pCible) {
        if(objetALaPosition(pCible) instanceof TNT) {
            tnt_count++;
            score += 100;
            grilleEntites[pCourant.x][pCourant.y] = null;
            if(checkLevelFinished()){
                levelCleared = true;
            }
        }
    }

    private void handleCrush(Entite e, Direction d, Point pCible) {
        Entite eCible = null;
        if((eCible = objetALaPosition(pCible)) != null && eCible.peutEtreEcrase()) {
            if(regarderDansLaDirection(eCible, d) == null) // la tuile d'après est vide
                deplacerEntite(pCible, calculerPointCible(pCible, d), eCible);
            else // la tuile d'après n'est pas vide
                if(eCible instanceof Heros)
                    GameOver();
                else if(eCible instanceof Bot) {
                    smicks.remove(eCible);
                    eCible = null;
            }
        }
    }

    private void deplacerEntite(Point pCourant, Point pCible, Entite e) {
        if(e instanceof EntiteDynamique) {
            ((EntiteDynamique) e).move(pCible.x - pCourant.x, pCible.y - pCourant.y);
            if(e instanceof Heros)
                handleTnt(pCourant, pCible);
        }
        else {
            grilleEntites[pCourant.x][pCourant.y] = null;
            grilleEntites[pCible.x][pCible.y] = e;
            map.put(e, pCible);
        }
    }
    
    /** Indique si p est contenu dans la grille
     */
    private boolean contenuDansGrille(Point p) {
        return p.x >= 0 && p.x < SIZE_X && p.y >= 0 && p.y < SIZE_Y;
    }
    
    public Entite cetteTile(EntiteDynamique e){
        return objetALaPosition(positionDeLObjet(e));
    }

    public Point positionDeLObjet(Entite e) {
        if(e instanceof EntiteDynamique)
            return ((EntiteDynamique) e).getTileIndex();
        return map.get(e);
    }

    public Entite objetALaPosition(Point p) {
        Entite retour = null;
        
        if (contenuDansGrille(p)) {
            retour = grilleEntites[p.x][p.y];
            if(retour == null) {
                Point ePos = hector.getPos();
                if(ePos.x == p.x && ePos.y == p.y) //on test pour le joueur
                    retour = hector;
                else //on test pour les smicks
                    for(Bot smick : smicks) {
                        ePos = smick.getPos();
                        if(ePos.x == p.x && ePos.y == p.y) {
                            retour = smick;
                            break;
                        }
                    }
                if(toyos != null && retour == null) // on test pour les toyo
                    for(Colonne col : toyos) {
                        ePos = col.getPos();
                        if(ePos.x == p.x && ePos.y == p.y) {
                            retour = col;
                            break;
                        }
                    }
            }
        }
        
        return retour;
    }

    public boolean checkLevelFinished() {
        return tnt_count == total_tnt;
    }

    public Ordonnanceur getOrdonnanceur() {
        return ordonnanceur;
    }

    private void GameOver() {
        gameIsOver = true;
    }

    public boolean getGameOver(){
        return gameOver;
    }
}
