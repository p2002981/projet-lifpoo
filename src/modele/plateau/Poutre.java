package modele.plateau;

public class Poutre extends EntiteStatique {
    private boolean estH;

    Poutre(Jeu jeu_, boolean _estH) {
        super(jeu_);
        estH = _estH;
    }   

    public boolean estHorizontale() {
        return estH;
    }
}
