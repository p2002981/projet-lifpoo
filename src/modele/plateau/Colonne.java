package modele.plateau;

public class Colonne extends EntiteDynamique {
    private boolean bleu;

    public Colonne(Jeu _jeu, int _x, int _y, boolean _estBleu) { 
        super(_jeu, _x, _y); 
        bleu = _estBleu;
    }

    public boolean peutEtreEcrase() { return false; }
    public boolean peutServirDeSupport() { return true; }
    public boolean peutPermettreDeMonterDescendre() { return false; };

    public boolean estBleu() {
        return bleu;
    }
}
