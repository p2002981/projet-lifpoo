package modele.plateau;

public class Corde extends EntiteStatique {
    Corde(Jeu jeu_) {
        super(jeu_);
    }

    @Override
    public boolean peutPermettreDeMonterDescendre() {
        return true;
    }

    public boolean peutServirDeSupport() {
        return false;
    }
}
