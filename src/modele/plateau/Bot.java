/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

import java.util.Random;

import modele.deplacements.IA;
/**
 * Ennemis (Smicks)
 */
public class Bot extends EntiteDynamique {
    private Random r = new Random();
    private IA intbot;

    public Bot(Jeu _jeu, int _x, int _y) {
        super(_jeu, _x, _y);
        intbot = new IA(this);
    }

    public boolean peutEtreEcrase() { return true; }
    public boolean peutServirDeSupport() { return true; }
    public boolean peutPermettreDeMonterDescendre() { return false; };
    public IA IAdeplacement(){
        return intbot;
    }
    public int randomizer(int min, int max){
        return r.nextInt(max-min+1)+min;
    }

}
