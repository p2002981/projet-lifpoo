package modele.plateau;

public class TNT extends EntiteStatique {
    public TNT(Jeu _jeu) {
        super(_jeu);
    }

    @Override
    public boolean peutServirDeSupport() {return false;}
}
