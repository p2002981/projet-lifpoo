package modele.plateau;

import modele.deplacements.Direction;
import java.awt.Point;

/**
 * Entités amenées à bouger (colonnes, ennemis)
 */
public abstract class EntiteDynamique extends Entite {
    private Point pos = new Point();
    private boolean estEnTrainDeMonter;

    public EntiteDynamique(Jeu _jeu, int _x, int _y) { 
        super(_jeu); 
        pos.x = _x;
        pos.y = _y;
    }

    public boolean avancerDirectionChoisie(Direction d) {
        return jeu.deplacerEntite(this, d);
    }
    public Entite regarderDansLaDirection(Direction d) {return jeu.regarderDansLaDirection(this, d);}

    public Entite regardeEnDiagonale(Direction gd, Direction hb){
        return jeu.regardeEnDiagonale(this, gd, hb);
    }
    
    public Point getPos() {
        return pos;
    }

    public Point getTileIndex() {
        return pos;
    }

    public void move(int x, int y) {
        pos.translate(x, y);
    }


    public Entite currentTile(){
        return jeu.cetteTile(this);
    }

    public boolean getEnTrainDeMonter(){
        return estEnTrainDeMonter;
    }

    public void setEnTrainDeMonter(boolean monte){
        estEnTrainDeMonter=monte;
    }
}
