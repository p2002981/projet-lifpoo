package modele.deplacements;


import javax.lang.model.util.ElementScanner14;

import modele.plateau.Bot;
import modele.plateau.Entite;

public class IA extends RealisateurDeDeplacement {
    private Direction directionCourante;
    private Bot e;
    private boolean dejaFait;

    public IA(Bot ennemBot){
        e=ennemBot;
        //addEntiteDynamique(ennemBot);
        directionCourante= Direction.gauche;
        dejaFait=false;
    }

    public Direction getDirectionCourante(){
        if (directionCourante==null)
            directionCourante=Direction.droite;
        return directionCourante;
    }

    public void setDirectionCourante(Direction direction){
        directionCourante=direction;
    }

    private boolean choixDep(Direction directionChoisie, boolean ret){
        Entite ebasDiago= e.regardeEnDiagonale(directionChoisie, Direction.bas); 
        Entite eCote = e.regarderDansLaDirection(directionChoisie);
        if(((eCote!=null && eCote.peutPermettreDeMonterDescendre()) || ebasDiago!=null) && e.avancerDirectionChoisie(directionChoisie)){
            ret=true;
        }
        else if (ebasDiago==null || !eCote.peutServirDeSupport()){
            dejaFait=false;
            choixDejaFait();
            //if((ebasDiago= e.regardeEnDiagonale(directionChoisie, Direction.bas))!=null && (eCote=e.regarderDansLaDirection(directionChoisie))!=null)
            //    choixDejaFait();
            //else
                ret= e.avancerDirectionChoisie(directionCourante);
        }
            
        return ret;
    }

    private void choixDejaFait(){
        if(!dejaFait){
            if(directionCourante == Direction.droite){
                setDirectionCourante(Direction.gauche);
            }
            else if (directionCourante == Direction.gauche){
                setDirectionCourante(Direction.droite);
            }
            dejaFait=true;
        }
    }


    private boolean paterneMonterCorde(){
        Entite eHaut= e.regarderDansLaDirection(Direction.haut);
        int rand =e.randomizer(0, 2);
        Entite eBas= e.regarderDansLaDirection(Direction.bas);
        //if((e.randomizer(0, 1)==0 /*|| e.getEnTrainDeMonter()) && e.currentTile().peutPermettreDeMonterDescendre()*/))
            if(rand == 1 &&(e.currentTile() != null && e.currentTile().peutPermettreDeMonterDescendre() && eHaut!=null)){
                e.setEnTrainDeMonter(true);
                setDirectionCourante(Direction.haut);
            }
            else if(eHaut==null || rand == 2) {//!eHaut.peutPermettreDeMonterDescendre()){
                switch (e.randomizer(0, 1)){
                    case 0:
                        setDirectionCourante(Direction.gauche);
                    break;
                    case 1:
                        setDirectionCourante(Direction.droite);
                    break;
                    }
                e.setEnTrainDeMonter(false);
            }
            else if (eBas!=null && eBas.peutPermettreDeMonterDescendre())
                setDirectionCourante(Direction.bas);
            return e.avancerDirectionChoisie(directionCourante);
    }


    protected boolean realiserDeplacement() {
        boolean ret=false;
        if(e.currentTile()==null || !e.currentTile().peutPermettreDeMonterDescendre()){
            e.setEnTrainDeMonter(false);
        }
        else{
            e.setEnTrainDeMonter(true);
            if(directionCourante!= Direction.bas)
                setDirectionCourante(Direction.haut);
        }
        switch(directionCourante){
            case droite:
            case gauche:
                choixDep(directionCourante, ret);
            break;
            case bas:
                if(e.regarderDansLaDirection(Direction.bas)!=null)
                    e.avancerDirectionChoisie(directionCourante);
                else{
                    switch(e.randomizer(0, 2)){
                        case 0:
                            e.setEnTrainDeMonter(false);
                        break;
                        case 1:
                            e.setEnTrainDeMonter(false);
                            setDirectionCourante(Direction.gauche);
                            choixDep(directionCourante, ret);
                        break;
                        case 2:
                            e.setEnTrainDeMonter(false);
                            setDirectionCourante(Direction.droite);
                            choixDep(directionCourante, ret);
                        break;
                    }
                }
            case haut:
                // on ne peut pas sauter sans prendre appui
                // (attention, test d'appui réalisé à partir de la position courante, si la gravité à été appliquée, il ne s'agit pas de la position affichée, amélioration possible)
                ret =paterneMonterCorde();
            break;
        }
        return ret;
    }

    public Direction getDirection(){
        return directionCourante;
    }
}
