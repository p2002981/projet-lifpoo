package modele.deplacements;

import modele.plateau.Heros;
import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;

/**
 * Controle4Directions permet d'appliquer une direction (connexion avec le clavier) à un ensemble d'entités dynamiques
 */
public class Controle4Directions extends RealisateurDeDeplacement {
    private Direction directionCourante;
    // Design pattern singleton
    private static Controle4Directions c3d;

    public static Controle4Directions getInstance() {
        if (c3d == null) {
            c3d = new Controle4Directions();
        }
        return c3d;
    }

    public void setDirectionCourante(Direction _directionCourante) {
        directionCourante = _directionCourante;
    }


    public boolean realiserDeplacement() {
        boolean ret = false;
        EntiteDynamique e= lstEntitesDynamiques.get(0);
        if (directionCourante != null)
            switch (directionCourante) {
                case gauche:
                case droite:
                    if (e.avancerDirectionChoisie(directionCourante)){
                        if(((Heros) e).currentTile()!= null && ((Heros) e).currentTile().peutPermettreDeMonterDescendre()){
                            ((Heros) e).setEnTrainDeMonter(true);
                        }
                        else
                            ((Heros) e).setEnTrainDeMonter(false);
                        ret = true;
                    }
                    break;

                case haut:
                    // on ne peut pas sauter sans prendre appui
                    // (attention, test d'appui réalisé à partir de la position courante, si la gravité à été appliquée, il ne s'agit pas de la position affichée, amélioration possible)
                    if (e instanceof Heros &&((Heros)e).currentTile()!=null && ((Heros) e).currentTile().peutPermettreDeMonterDescendre()){
                        ((Heros) e).setEnTrainDeMonter(true);
                        if(e.regarderDansLaDirection(Direction.haut)== null)
                            e.setEnTrainDeMonter(false);
                        ret = e.avancerDirectionChoisie(Direction.haut);
                    }
                break;

                case bas:
                    Entite eBas=e.regarderDansLaDirection(Direction.bas);
                    if(eBas!=null && eBas.peutPermettreDeMonterDescendre())
                        e.avancerDirectionChoisie(Direction.bas);
            }
        return ret;
    }

    public void resetDirection() {
        directionCourante = null;
    }
}
