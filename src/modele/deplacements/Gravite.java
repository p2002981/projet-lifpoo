package modele.deplacements;

import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;

public class Gravite extends RealisateurDeDeplacement {
    @Override
    public boolean realiserDeplacement() {
        boolean ret = false;

        for (EntiteDynamique e : lstEntitesDynamiques) {
            Entite eBas = e.regarderDansLaDirection(Direction.bas);
            if (e instanceof EntiteDynamique && ((EntiteDynamique) e).getEnTrainDeMonter())
                ret=true;
            else if (eBas == null || (eBas != null && eBas.peutPermettreDeMonterDescendre())) {
                ret = e.avancerDirectionChoisie(Direction.bas);
            }
        }

        return ret;
    }
}
