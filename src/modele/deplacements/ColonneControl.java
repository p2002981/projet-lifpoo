package modele.deplacements;

import modele.plateau.EntiteDynamique;

/**
 * A la reception d'une commande, toutes les cases (EntitesDynamiques) des colonnes se déplacent dans la direction définie
 * (vérifier "collisions" avec le héros)
 */
public class ColonneControl extends RealisateurDeDeplacement {
    private static ColonneControl toyoRougeDeplacement;
    private static ColonneControl toyoBleuDeplacement;

    private Direction dir;
    private int height;

    public static ColonneControl getRedInstance() {
        if(toyoRougeDeplacement == null)
            toyoRougeDeplacement = new ColonneControl();
        return toyoRougeDeplacement;
    }

    public static ColonneControl getBlueInstance() {
        if(toyoBleuDeplacement == null)
            toyoBleuDeplacement = new ColonneControl();
        return toyoBleuDeplacement;
    }

    public void addEntiteDynamique(EntiteDynamique ed, boolean estEnBas) {
        lstEntitesDynamiques.add(ed);
        height = estEnBas?0:3;
        dir = estEnBas?Direction.bas:Direction.haut;
    };

    public void toggleDirection() {
        if(dir == Direction.bas)
            dir = Direction.haut;
        else
            dir = Direction.bas;
    }

    protected boolean realiserDeplacement() { 
        if(dir == Direction.bas) {
            if(height > 1) {
                for(EntiteDynamique e : lstEntitesDynamiques) {
                    e.avancerDirectionChoisie(dir);
                }
                height--;
                return true;
            }
        }
        else if(dir == Direction.haut) { 
            if(height < 3) {
                for(EntiteDynamique e : lstEntitesDynamiques) {
                    e.avancerDirectionChoisie(dir);
                    
                }
                height++;
                return true;
            }
        }
        return false;
    }
}
